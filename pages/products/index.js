
import useSWR from "swr";

const fetcher = (...args) => fetch(...args).then((res) => res.json());

const Products = () => {
    const {data, error} = useSWR('http://localhost:5000/products', fetcher);

    if(error) return <div>Failed to load</div>
    if(!data) return <div>Loading...</div>

    return <div>
        {data.map((item) => (
            <ul key={item.id}>
                <li>{item.name} - {item.price}</li>
            </ul>
        ))}
    </div>;
}

export default Products;


